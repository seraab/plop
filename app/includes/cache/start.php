<?php 

	/***	
	Copyright (C) <2019>  <Thomas Raab>

	This program is free software: you can redistribute it and/or modify
	it under the terms of the GNU General Public License as published by
	the Free Software Foundation, either version 3 of the License, or
 	(at your option) any later version.

	This program is distributed in the hope that it will be useful,
	but WITHOUT ANY WARRANTY; without even the implied warranty of
	MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
	GNU General Public License for more details.

	You should have received a copy of the GNU General Public License
	along with this program.  If not, see <https://www.gnu.org/licenses/>.

	Please send all Inquires to: tom@ploppityplop.com

	***/
	
	$cachedir = $GLOBALS["APP_ROOT"]."/includes/cache/files";
	 // create new directory with 744 permissions if it does not exist yet
	 // owner will be the user/group the PHP script is run under
	if ( !file_exists($cachedir) ) {
		mkdir ($cachedir, 0755);
	}
		
	$cachefile = $GLOBALS["APP_ROOT"]."/includes/cache/files/".$cacheKey; 	$cachetime = $cache_override * 60;	// Serve from the cache if it is younger than $cachetime	if (file_exists($cachefile) && time() - $cachetime < filemtime($cachefile)) {	    include($cachefile);	    echo "<!-- Using Cache file all other processing is halted -->\n";	    exit;	}else{
		// this has to be called before any header data is sent		ob_start(); // Start the output buffer
	}
?>